# Http Basic User Roles

## References
```ref
1. https://docs.spring.io/spring-security/site/docs/current/reference/html5
```

## L57 - Introduction
```
1. User roles define user can do what in the apps.
2. Roles are used for AUTHORIZATION .
3. For role related restriction we  get 403 - Forbidden
```

## L58 - Authorization in Spring Security
```
1. refer ./L58-AuthorizationInSpringSec.pdf
```

## L59 -  Configuring user roles
```
1. refer ./L59-ConfigUserRoles.pdf
```

## L60 - Verify Role Tests
```
Use case 

  - /role/admins/** should be accessed be "ROLE_ADMIN" only .
1. When we save role in db pass role name prefix as ROLE_ .
  - refer - com.udemy.sfg.s09httpbasicauth.bootstrap.UserDataLoader

2. Modify Security configuarion to restrict api as per specific role
   -
   http.authorizeRequests((requests) -> {
            requests
                    // for admin APIs
                    .antMatchers("/role/admins/**").hasRole("ADMIN")
            ;
        });
  - refer - com.udemy.sfg.s09httpbasicauth.configs.SecurityConfig -> configure(HttpSecurity)
  - please note here we must not prefix "ROLE_"
  
  - JUnit Tests
    -- com.udemy.sfg.s09httpbasicauth.controller.roles.AdminControllerTests  
```

## Assignment - Restric customers API to ROLE_CUSTOMER only
```
use case
  - /role/customers/** should be accessed be "ROLE_CUSTOMER"  only .
  - "ROLE_ADMIN" will get access of this during L61
1. Modify Security configuarion to restrict api as per specific role
   -
   http.authorizeRequests((requests) -> {
            requests
                    // for admin APIs
                    .antMatchers("/role/customers/**").hasRole("ADMIN") // or .antMatchers("/role/customer**").hasRole("ADMIN") 
            ;
        });
  - refer - com.udemy.sfg.s09httpbasicauth.configs.SecurityConfig -> configure(HttpSecurity)
  - please note here we must not prefix "ROLE_"
  
  - JUnit Tests
    -- com.udemy.sfg.s09httpbasicauth.controller.roles.CustomerControllerTests
```

## L61 - Allow multiple Roles
```
use case
  - /role/customers/** should be accessed be "ROLE_CUSTOMER" as well as "ROLE_ADMIN" .

1. To allow single role we use hasRole(String) 
   but in order to allow multiple role we use hasAnyRole(String...)
   http.authorizeRequests((requests) -> {
            requests.antMatchers("/role/customer**").hasAnyRole("ADMIN","CUSTOMER")
            ;
        });
  - refer - SecurityConfig ->configure(HttpSecurity)
  - JUnit 
    -- Commented CustomerControllerTests -> testGetAPI_AS_AdminRoleUser_ValidCredential_403()
```

## L62 - Refactoring JUnit
```
```

## Assignment
```
use case
  - /role/users/** should be accessed be "ROLE_CUSTOMER" , "ROLE_ADMIN" and "ROLE_USER" .

1. To allow single role we use hasRole(String) 
   but in order to allow multiple role we use hasAnyRole(String...)
   http.authorizeRequests((requests) -> {
            requests.antMatchers("/role/users**").hasAnyRole("ADMIN","CUSTOMER","USER")
            ;
        });
   - refer - SecurityConfig -> configure(HttpSecurity)
   - JUnit tests
     -- UserControllerTests 
```

## L63 - Intro to method security - @Secured
```
1. PREQISITE
- whenever we want to secure method we need to make sure the method is not restricted to a specific role
  as part of SecurityConfig -> configure(HttpSecurity)
- for eg we need to secure {baseEndpoint}/role/admins/users api on method level
  then on security config class we CANNOT have config as /role/admins/** as all
  the child method are already secured to a role
  
  we need to give specific path to security config, and for all othe methods of parent /role/admin 
  can be secured at method level
    -- refer AdminController -> userAndAdminRoleAccessInAdmin()
    -- refer SecurityConfig -> configure(HttpSecurity)
       --- for resticting parent path of AdminController -> userAndAdminRoleAccessInAdmin()
    
    
    http.authorizeRequests((requests) -> {
            requests
                    .antMatchers("/role/admins")
                      .hasRole("ADMIN")
            ;
        });
        
2. Steps
  - step1 - add @EnableGlobalMethodSecurity(securedEnabled = true) on SecurityConfig
    -- refer - https://www.baeldung.com/spring-security-method-security
    
  - step2 - add @Secured({String roles}) on the method which we want to secure
    -- refer AdminController -> userAndAdminRoleAccessInAdmin()
    -- roles MUST BE prefixed with ROLE_

3. Spring uses AOP to enable this method level security.
```

## L64 - Intro to method security using @PreAuthorize
```
1. PREQISITE
- whenever we want to secure method we need to make sure the method is not restricted to a specific role
  as part of SecurityConfig -> configure(HttpSecurity)
- for eg we need to secure {baseEndpoint}/role/admins/users api on method level
  then on security config class we CANNOT have config as /role/admins/** as all
  the child method are already secured to a role
  
  we need to give specific path to security config, and for all othe methods of parent /role/admin 
  can be secured at method level
    -- refer AdminController -> customerAndAdminRoleAccessInAdmin()
    -- refer SecurityConfig -> configure(HttpSecurity)
       --- for resticting parent path of AdminController -> customerAndAdminRoleAccessInAdmin()
    
    
    http.authorizeRequests((requests) -> {
            requests
                    .antMatchers("/role/admins")
                      .hasRole("ADMIN")
            ;
        });
        
2. Steps
  - step1 - add @EnableGlobalMethodSecurity(prePostEnabled = true) on SecurityConfig
    -- refer - https://www.baeldung.com/spring-security-method-security
    
  - step2 - add @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN','CUSTOMER')") on the method which we want to secure
    -- refer AdminController -> customerAndAdminRoleAccessInAdmin()
    -- roles prefixed with ROLE_ is OPTIONAL

3. Method level security using @Secured vs @PreAuthorize
  - @EnableGlobalMethodSecurity
    -- to enable @Secured use @EnableGlobalMethodSecurity(securedEnabled = true)
    -- to enable @Secured use @EnableGlobalMethodSecurity(prePostEnabled = true)
  - String
    - @Secured takes plain String as @Secured({"ROLE_ADMIN", "ROLE_USER"})
    - @PreAuthorize takes SpEL as @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN','CUSTOMER')")
  - prefix ROLE_
    -  for @Secured it's MUST
    -  for @PreAuthorize it's OPTIONAL

4. Spring uses AOP to enable this method level security.
```

