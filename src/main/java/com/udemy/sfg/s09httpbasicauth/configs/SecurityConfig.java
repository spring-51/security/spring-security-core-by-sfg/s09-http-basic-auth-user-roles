package com.udemy.sfg.s09httpbasicauth.configs;

import com.udemy.sfg.s09httpbasicauth.factories.XyzPasswordEncoderFactories;
import com.udemy.sfg.s09httpbasicauth.security.filters.RestHeaderAuthenticationFilter;
import com.udemy.sfg.s09httpbasicauth.security.filters.RestUrlParamAuthenticationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@RequiredArgsConstructor
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    // custom filters - start

    public RestHeaderAuthenticationFilter restHeaderAuthenticationFilter(AuthenticationManager authenticationManager){
        RestHeaderAuthenticationFilter filter
                = new RestHeaderAuthenticationFilter(new AntPathRequestMatcher("/**")); // ant matcher tells filter
                                                                                               // that  on what urls this
                                                                                               // filter will work
        // this authentication manager is responsible for this filter
        filter.setAuthenticationManager(authenticationManager);
        return filter;
    }

    public RestUrlParamAuthenticationFilter restUrlParamAuthenticationFilter(AuthenticationManager authenticationManager){
        RestUrlParamAuthenticationFilter filter
                = new RestUrlParamAuthenticationFilter(new AntPathRequestMatcher("/**"));

        filter.setAuthenticationManager(authenticationManager);
        return filter;

    }

    // custom filters - end

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.addFilterBefore(
                restHeaderAuthenticationFilter(authenticationManager()),
                UsernamePasswordAuthenticationFilter.class)
        ;

        http.addFilterBefore(
                restUrlParamAuthenticationFilter(authenticationManager()),
                UsernamePasswordAuthenticationFilter.class)
        ;

        http.csrf().disable() // in the tutorial w/o this it was not working
                          // but in this app w/o this also it's working
                          // I kept it to keep it sync with tutorials
        ;

        // requests.antMatchers(..) is used for whitelisting APIs
        // order of ant matcher is important
        // we can place requests.antMatchers(..) after requests.anyRequest()
        // it will give RTE as java.lang.IllegalStateException: Can't configure antMatchers after anyRequest
        http.authorizeRequests((requests) -> {
            requests
                    // this will whitelist api for h2 -console
                    .antMatchers("/h2-console/**").permitAll()
                    // this will whitelist api for all Http methods
                    .antMatchers("/whitelist/public", "/").permitAll()

                    // this will whitelist api for ONLY Http GET
                    .antMatchers(HttpMethod.GET, "/whitelist/get").permitAll()

                    // this will whitelist api for ONLY Http GET, syntax of string matches with @RequestMapping
                    // for such syntax of string we use mvcMatchers(..) in place of antMatchers(..)
                    .mvcMatchers(HttpMethod.GET, "/whitelist/{path}").permitAll()
                     // for admin APIs
                     //.antMatchers("/admin/**").hasRole("ROLE_ADMIN") // this will fail since we can't prefix ROLE_ here
                    .antMatchers("/role/admins") // here we removed /** since we are using method level security
                      .hasRole("ADMIN")
                    // for customer APIs
                    //.antMatchers("/admin/**").hasRole("ROLE_CUSTOMER") // this will fail since we can't prefix ROLE_ here
                    //.antMatchers("/role/customer**").hasRole("CUSTOMER") // or .antMatchers("/role/customers/**").hasRole("CUSTOMER")
                                                                           // to give access to EXACT ONE role
                    .antMatchers("/role/customer**").hasAnyRole("ADMIN","CUSTOMER")
                    // for user APIs
                    .antMatchers("/role/users**").hasAnyRole("ADMIN","CUSTOMER","USER")
            ;
        });
        http.authorizeRequests((requests) -> {
            ((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)requests.anyRequest()).authenticated();
        });

        http.formLogin();
        http.httpBasic();

        // iframe option
        // h2 console uses iframes
        http.headers().frameOptions().sameOrigin();
    }

    @Override
    @Bean("inMemUserDetailsService")
    protected UserDetailsService userDetailsService() {
        UserDetails admin = User.withDefaultPasswordEncoder()
                .username("inmem")
                .password("inmem")
                .roles("INMEM")
                .build();

        UserDetails user = User.withDefaultPasswordEncoder()
                .username("inmem2")
                .password("inmem2")
                .roles("INMEM2")
                .build();
        return new InMemoryUserDetailsManager(admin,user);
    }

    private final UserDetailsService userDetailsService;
    private final UserDetailsService user2DetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        // in memory authentication - start
        // commented since we are using Db authentication
        /*

        auth.inMemoryAuthentication()
                .withUser("noopuser")
                //.password("noopuser") // commented since we are not using NoOpPasswordEncoder
                //.password(passwordEncoder().encode("noopuser")) // commented since we using Delegating password encoder
                                                                  // if we are using speicific encoder like ldp, sha256, bcrypt
                                                                  // then this line shouldn't be commented
                                                                  // refer StudentControllerTests.testHttpBasicInMemAuthNoOpEncodedCredentialUser()
                .password("{noop}noopuser")
                .roles("NOOPUSER")
                .and()
                    .withUser("ldapuser")
                    //.password("ldapuser") // commented since we are not using NoOpPasswordEncoder
                    //.password(passwordEncoder().encode("ldapuser")) // commented since we using Delegating password encoder
                                                                      // if we are using speicific encoder like ldp, sha256, bcrypt
                                                                      // then this line shouldn't be commented
                                                                      // refer StudentControllerTests.testHttpBasicInMemAuthLdapEncodedCredentialUser()
                    .password("{ldap}{SSHA}ZvonpN7PIjlNWtPl/TuLNYfhd8mV9+i+aAj46g==") // ldap encoded string for ldapuser as password
                    .roles("LDAPUSER")
                .and()
                    .withUser("sha256user")
                    //.password(passwordEncoder().encode("sha256user")) // commented since we using Delegating password encoder
                                                                        // if we are using speicific encoder like ldp, sha256, bcrypt
                                                                        // then this line shouldn't be commented
                                                                        // refer StudentControllerTests.testHttpBasicInMemAuthSha256EncodedCredentialUser()
                    .password("{sha256}7cdfefb9b4fc94fd6cb6a7702cac0402addbdbc3bad333a31db658b932c76d688eb5e9f5b6ce9b60")  // sha256 encoded string for sha256user as password
                    .roles("SHA256USER")
                .and()
                    .withUser("bcryptuser")
                    //.password(passwordEncoder().encode("bcryptuser")) // commented since we using Delegating password encoder
                                                                        // if we are using speicific encoder like ldp, sha256, bcrypt
                                                                        // then this line shouldn't be commented
                                                                        // refer StudentControllerTests.testHttpBasicInMemAuthBCryptEncodedCredentialUser()
                    .password("{bcrypt}$2a$10$xFOsfgTS7Ggx2b0fQiS0c.2XB3uX4TLUtSY1C2NhkrtxIebXovqkC")  // BCrypt encoded string for bcryptuser as password
                    .roles("BCRYPTUSER")
                ;

        // this will keep all the user and add this user to the list
        auth.inMemoryAuthentication()
                .withUser("bcrypt15user")
                //.password(passwordEncoder().encode("bcrypt15user")) // commented since we using Delegating password encoder
                // if we are using speicific encoder like ldp, sha256, bcrypt
                // then this line shouldn't be commented
                // refer StudentControllerTests.testHttpBasicInMemAuthBCrypt15EncodedCredentialUser()
                .password("{bcrypt15}$2a$15$6msZ1FXTRL06E.XL24DY7OPJHHCx5/RBqQlU2M7fCcUMHYU9.1aVi")  // BCrypt15 encoded string for bcrypt15user as password
                .roles("BCRYPT15USER");

         */
        // in memory authentication - End

        auth
                // take credential from CustomUser entity
                .userDetailsService(userDetailsService).passwordEncoder(passwordEncoder())
                .and()
                // take credential from CustomUser2 entity
                .userDetailsService(user2DetailsService).passwordEncoder(NoOpPasswordEncoder.getInstance())
                .and()
                // take credential from in memory
                .userDetailsService(userDetailsService())
        ;
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        // return NoOpPasswordEncoder.getInstance();
        // return new LdapShaPasswordEncoder();
        // return new StandardPasswordEncoder();
        // return new BCryptPasswordEncoder();
        //return PasswordEncoderFactories.createDelegatingPasswordEncoder();
        return XyzPasswordEncoderFactories.createDelegatingPasswordEncoder();
    }


}
