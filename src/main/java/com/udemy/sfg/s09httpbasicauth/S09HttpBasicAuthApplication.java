package com.udemy.sfg.s09httpbasicauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S09HttpBasicAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(S09HttpBasicAuthApplication.class, args);
    }
}
