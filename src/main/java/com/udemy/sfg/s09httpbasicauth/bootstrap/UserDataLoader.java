package com.udemy.sfg.s09httpbasicauth.bootstrap;

import com.udemy.sfg.s09httpbasicauth.security.dtos.CustomAuthority;
import com.udemy.sfg.s09httpbasicauth.security.dtos.CustomAuthority2;
import com.udemy.sfg.s09httpbasicauth.security.dtos.CustomUser;
import com.udemy.sfg.s09httpbasicauth.security.dtos.CustomUser2;
import com.udemy.sfg.s09httpbasicauth.security.repositories.CustomAuthority2Repository;
import com.udemy.sfg.s09httpbasicauth.security.repositories.CustomAuthorityRepository;
import com.udemy.sfg.s09httpbasicauth.security.repositories.CustomUser2Repository;
import com.udemy.sfg.s09httpbasicauth.security.repositories.CustomUserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Slf4j
@Component
@RequiredArgsConstructor
public class UserDataLoader implements CommandLineRunner {

    private final CustomUserRepository userRepository;
    private final CustomUser2Repository user2Repository;
    private final CustomAuthorityRepository authorityRepository;
    private final CustomAuthority2Repository authority2Repository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        CustomAuthority admin =  authorityRepository.save(CustomAuthority.builder()
                .role("ROLE_ADMIN")
                .build()
        );

        CustomAuthority2 noAccess =authority2Repository.save(CustomAuthority2.builder().role("ROLE_NOACCESS").build());

        CustomAuthority user = authorityRepository.save(CustomAuthority.builder()
                .role("ROLE_USER")
                .build()
        );

        CustomAuthority customer = authorityRepository.save(CustomAuthority.builder()
                .role("ROLE_CUSTOMER")
                .build()
        );

        CustomUser noopUser = CustomUser.builder()
                .authority(admin)
                .username("noopuser")
                .password(passwordEncoder.encode("noopuser"))
                .build();

        CustomUser ldapUser = CustomUser.builder()
                .authority(user)
                .username("ldapuser")
                .password(passwordEncoder.encode("ldapuser"))
                .build();

        CustomUser sha256User = CustomUser.builder()
                .authority(customer)
                .username("sha256user")
                .password(passwordEncoder.encode("sha256user"))
                .build();

        CustomUser bcryptUser = CustomUser.builder()
                .authority(customer)
                .username("bcryptuser")
                .password(passwordEncoder.encode("bcryptuser"))
                .build();

        CustomUser bcrypt15User = CustomUser.builder()
                .authority(customer)
                .username("bcrypt15user")
                .password(passwordEncoder.encode("bcrypt15user"))
                .build();

        userRepository.saveAll(Arrays.asList(noopUser, ldapUser, sha256User, bcryptUser, bcrypt15User));
        user2Repository.save(
                CustomUser2.builder()
                        .authority(noAccess)
                        .username("noopuser2")
                        .password(NoOpPasswordEncoder.getInstance().encode("noopuser2"))
                        .build()
        );
        log.debug("Users loaded ...............");

    }
}
