package com.udemy.sfg.s09httpbasicauth.security.dtos;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
public class CustomAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String role;

    @ManyToMany(
            mappedBy = "authorities" // name of variable in CustomUser entity which
                                     // is responsible for creating table for this @ManyToManay
    )
    private Set<CustomUser> users;
}
