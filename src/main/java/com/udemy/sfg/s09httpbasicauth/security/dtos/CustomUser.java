package com.udemy.sfg.s09httpbasicauth.security.dtos;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

/*
this will be same as Spring inter User class
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
public class CustomUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String password;
    private String username;

    @Singular
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(
            name = "user_authorities",
            joinColumns = {
                    @JoinColumn(name = "USER_ID",referencedColumnName = "ID") // what is  referencedColumnName ?
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "AUTHORITY_ID", referencedColumnName = "ID")
            }
    )
    private Set<CustomAuthority> authorities;
    @Builder.Default
    private boolean accountNonExpired = true;
    @Builder.Default
    private boolean accountNonLocked = true;
    @Builder.Default
    private boolean credentialsNonExpired = true;
    @Builder.Default
    private boolean enabled = true;
}
