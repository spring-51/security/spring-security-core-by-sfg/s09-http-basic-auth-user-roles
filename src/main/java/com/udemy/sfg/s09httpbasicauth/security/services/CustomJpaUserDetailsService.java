package com.udemy.sfg.s09httpbasicauth.security.services;

import com.udemy.sfg.s09httpbasicauth.security.dtos.CustomAuthority;
import com.udemy.sfg.s09httpbasicauth.security.dtos.CustomUser;
import com.udemy.sfg.s09httpbasicauth.security.repositories.CustomUserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service("userDetailsService")
@Slf4j
public class CustomJpaUserDetailsService implements UserDetailsService {

    private final CustomUserRepository userRepository;

    @Transactional // we used it since before we call user.getAuthorities(), hibernate closes the session
                   // if we comment @Transactional hibernate closes the session at after line 33 before line 36
                   // if we comment @Transactional such transaction is called IMPLICIT transaction
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.debug(String.format("CustomJpaUserDetailsService -> loadUserByUsername::::::::::::: %s",username));
        CustomUser user = userRepository.findByUsername(username)
                .orElseThrow(
                        () -> new UsernameNotFoundException(String.format("Username : {0} not found", username))
                );

        // this is Spring internal model class which
        // will be used for authentication
        User principal = new User(
                user.getUsername(),
                user.getPassword(),
                user.isEnabled(),
                user.isAccountNonExpired(),
                user.isCredentialsNonExpired(),
                user.isAccountNonLocked(),
                convertTOSpringInternalGrantedAuthority(user.getAuthorities())
                );
        return principal;
    }


    private Collection<? extends GrantedAuthority> convertTOSpringInternalGrantedAuthority(
            Set<CustomAuthority> authorities) {
        if(authorities != null && !authorities.isEmpty()){
            Set<SimpleGrantedAuthority> simpleGrantedAuthority = authorities.stream()
                    .map(CustomAuthority::getRole)
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toSet());
            return simpleGrantedAuthority;
        }
        return new HashSet<>();
    }
}
