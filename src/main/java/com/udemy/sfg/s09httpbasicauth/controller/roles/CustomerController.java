package com.udemy.sfg.s09httpbasicauth.controller.roles;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/role/customers")
public class CustomerController {
    @GetMapping
    public String getAPI(){
        return "Hello from CustomerController -> getAPI";
    }
}
