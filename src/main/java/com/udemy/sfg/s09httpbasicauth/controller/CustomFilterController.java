package com.udemy.sfg.s09httpbasicauth.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/filters/custom")
public class CustomFilterController {

    @GetMapping
    public String getAPI(){
        return "hello from CustomFilterController -> getAPI";
    }
}
