package com.udemy.sfg.s09httpbasicauth.controller.roles;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/role/admins")
public class AdminController {

    @GetMapping
    public String getAPI(){
        return "Hello from AdminController -> getAPI";
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping("/users")
    public String userAndAdminRoleAccessInAdmin(){
        return "Hello from AdminController -> userRoleAccessInAdmin";
    }

    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN','CUSTOMER')")
    @GetMapping("/customers")
    public String customerAndAdminRoleAccessInAdmin(){
        return "Hello from AdminController -> userRoleAccessInAdmin";
    }

}
