package com.udemy.sfg.s09httpbasicauth.controller.roles;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/role/users")
public class UserController {

    @GetMapping
    public String getAPI(){
        return "Hello from UserController -> getAPI";
    }
}
