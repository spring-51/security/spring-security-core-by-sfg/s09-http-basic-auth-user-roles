package com.udemy.sfg.s09httpbasicauth.controller;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// @WebMvcTest load minimum config for  mvc tests
// it does not load Spring Data JPA, Custom user details service etc
// that's why when we migrate spring sec to db authentication
// @WebMvcTest will not work, that's why we commented it use @SpringBootTest
// @WebMvcTest
@SpringBootTest
public class CustomFilterControllerTests extends BaseTests {

    // with custom header - Api-Key,Api-Secret - Start
    // here  http basic auth will be used for authentication where credential are passed as header keys

    @Test
    void getApiUsingCorrectCredentialAsHeaderTest() throws  Exception{
        mockMvc.perform(
                    get("/filters/custom")
                            .header("Api-Key","noopuser")
                            .header("Api-Secret", "noopuser")
                ).andExpect(status().isOk());
    }

    @Test
    void getApiUsingBadCredentialAsHeaderTest() throws  Exception{
        mockMvc.perform(
                get("/filters/custom")
                        .header("Api-Key","noopuser")
                        .header("Api-Secret", "incorrectpassword")
        ).andExpect(status().isUnauthorized());
    }

    // with custom header - Api-Key,Api-Secret - End

    // with query params - Api-Key,Api-Secret - Start

    @Test
    void getApiUsingCorrectCredentialAsQueryParamTest() throws  Exception{
        mockMvc.perform(
                //get("/filters/custom?Api-Key=noopuser&Api-Secret=noopuser")
                get("/filters/custom")
                        .param("apiKey", "noopuser")
                        .param("apiSecret","noopuser")
                 ).andExpect(status().isOk());
    }

    @Test
    void getApiUsingBadCredentialAsQueryParamTest() throws  Exception{
        mockMvc.perform(
                get("/filters/custom?apiKey=noopuser&apiSecret=incorrectCredential")
        ).andExpect(status().isUnauthorized());
    }

    // with query params - Api-Key,Api-Secret - End

    // without custom header - Api-Key,Api-Secret - Start
    // here  http basic auth will be used for authentication where credential are passed as base64Encode(Basic username:password)
    @Test
    void getApiWithoutCustomHeaderTest() throws  Exception{
        mockMvc.perform(
                get("/filters/custom")
                .with(httpBasic("noopuser", "noopuser"))
        ).andExpect(status().isOk());
    }
    // without custom header - Api-Key,Api-Secret - End
}
