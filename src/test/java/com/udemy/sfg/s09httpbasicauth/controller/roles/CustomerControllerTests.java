package com.udemy.sfg.s09httpbasicauth.controller.roles;

import com.udemy.sfg.s09httpbasicauth.controller.BaseTests;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.Stream;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class CustomerControllerTests extends BaseTests {

    /*
     noopuser - is ROLE_ADMIN,
     ldapuser - is ROLE_USER,
     sha256user - ROLE_CUSTOMER,
     bcryptuser - ROLE_CUSTOMER,
     bcrypt15user - ROLE_CUSTOMER

       refer - com.udemy.sfg.s09httpbasicauth.bootstrap.UserDataLoader
       -- there we are assigning role to above specified user
     */

    static private final String ADMIN_ROLE_USER_USERNAME= "noopuser";
    static private final String ADMIN_ROLE_USER_PASSWORD= ADMIN_ROLE_USER_USERNAME;

    static private final String CUSTOMER_ROLE_USER_USERNAME= "sha256user";
    static private final String CUSTOMER_ROLE_USER_PASSWORD= CUSTOMER_ROLE_USER_USERNAME;

    static private final String USER_ROLE_USER_USERNAME= "ldapuser";
    static private final String USER_ROLE_USER_PASSWORD= USER_ROLE_USER_USERNAME;

    static private final String INCORRECT_USERNAME= "incorrectusername";
    static private final String INCORRECT_PASSWORD= INCORRECT_USERNAME;


    // it was written for L60
    // commented from L61
    /*
    @Test
    void testGetAPI_AS_AdminRoleUser_ValidCredential_403() throws Exception{
        mockMvc.perform(get("/role/customers")
                .with(httpBasic(ADMIN_ROLE_USER_USERNAME, ADMIN_ROLE_USER_PASSWORD)))
                .andExpect(status().isForbidden());
    }

     */

    @ParameterizedTest(name = "#{index} with [{arguments}]")
    @MethodSource("com.udemy.sfg.s09httpbasicauth.controller.roles.CustomerControllerTests#getStreamAllAuthorizedRoleUsers") // packageName.className#staticMethodToSupplyArgs
    void testGetAPI_ValidCredential_200(String username, String password) throws Exception{
        mockMvc.perform(get("/role/customers")
                .with(httpBasic(username, password)))
                .andExpect(status().isOk());
    }

    @Test
    void testGetAPI_AS_UserRoleUser_ValidCredential_403() throws Exception{
        mockMvc.perform(get("/role/customers")
                .with(httpBasic(USER_ROLE_USER_USERNAME, USER_ROLE_USER_PASSWORD)))
                .andExpect(status().isForbidden());
    }

    @Test
    void testGetAPI_InvalidCredential_401() throws Exception{
        mockMvc.perform(get("/role/customers")
                .with(httpBasic(INCORRECT_USERNAME, INCORRECT_PASSWORD)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void testGetAPI_NoAuth_401() throws Exception {
        mockMvc.perform(get("/role/admins"))
                .andExpect(status().isUnauthorized());
    }

    private static Stream<Arguments> getStreamAllAuthorizedRoleUsers() {
        return Stream.of(
                Arguments.of(CUSTOMER_ROLE_USER_USERNAME, CUSTOMER_ROLE_USER_PASSWORD),
                Arguments.of(ADMIN_ROLE_USER_USERNAME, ADMIN_ROLE_USER_PASSWORD)
        );
    }
}
