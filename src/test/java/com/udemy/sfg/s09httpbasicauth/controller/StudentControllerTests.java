package com.udemy.sfg.s09httpbasicauth.controller;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// @WebMvcTest load minimum config for  mvc tests
// it does not load Spring Data JPA, Custom user details service etc
// that's why when we migrate spring sec to db authentication
// @WebMvcTest will not work, that's why we commented it use @SpringBootTest
// @WebMvcTest
@SpringBootTest
public class StudentControllerTests extends BaseTests {


    // username and password in optional, but annotation is mandatory
    // @WithMockUser(username = "anything", password = "anything")
    // @WithMockUser(username = "anything")
    @WithMockUser
    @Test
    void getStudents() throws Exception{
        mockMvc.perform(get("/students"))
                .andExpect(status().isOk());
    }

    // using credential from CustomUser entity - start
    @Test
    void testHttpBasicDbAuthNoOpEncodedCredentialUserFromCustomUserEntity() throws Exception{
        mockMvc.perform(get("/students")
                // this will fail because we specified differ credential in main -> application.properties
                //.with(httpBasic("testuser", "testpassword")))
                // this will pass because we specified same credential in main -> application.properties
                .with(httpBasic("noopuser", "noopuser")))
                .andExpect(status().isOk());
    }

    @Test
    void testHttpBasicDbAuthLdapEncodedCredentialUserFromCustomUserEntity() throws Exception{
        mockMvc.perform(get("/students")
                // this will fail because we specified differ credential in main -> application.properties
                //.with(httpBasic("testuser", "testpassword")))
                // this will pass because we specified same credential in main -> application.properties
                .with(httpBasic("ldapuser", "ldapuser")))
                .andExpect(status().isOk());
    }

    @Test
    void testHttpBasicDbAuthSha256EncodedCredentialUserFromCustomUserEntity() throws Exception{
        mockMvc.perform(get("/students")
                // this will fail because we specified differ credential in main -> application.properties
                //.with(httpBasic("testuser", "testpassword")))
                // this will pass because we specified same credential in main -> application.properties
                .with(httpBasic("sha256user", "sha256user")))
                .andExpect(status().isOk());
    }

    @Test
    void testHttpBasicDbAuthBCryptEncodedCredentialUserFromCustomUserEntity() throws Exception{
        mockMvc.perform(get("/students")
                // this will fail because we specified differ credential in main -> application.properties
                //.with(httpBasic("testuser", "testpassword")))
                // this will pass because we specified same credential in main -> application.properties
                .with(httpBasic("bcryptuser", "bcryptuser")))
                .andExpect(status().isOk());
    }

    @Test
    void testHttpBasicDbAuthBCrypt15EncodedCredentialUserFromCustomUserEntity() throws Exception{
        mockMvc.perform(get("/students")
                // this will fail because we specified differ credential in main -> application.properties
                //.with(httpBasic("testuser", "testpassword")))
                // this will pass because we specified same credential in main -> application.properties
                .with(httpBasic("bcrypt15user", "bcrypt15user")))
                .andExpect(status().isOk());
    }

    // using credential from CustomUser entity - end
    // using credential from CustomUser2 entity - Start

    @Test
    void testHttpBasicDbAuthFromCustomUser2Entity() throws Exception{
        mockMvc.perform(get("/students")
                // this will fail because we specified differ credential in main -> application.properties
                //.with(httpBasic("testuser", "testpassword")))
                // this will pass because we specified same credential in main -> application.properties
                .with(httpBasic("noopuser2", "noopuser2")))
                .andExpect(status().isOk());
    }

    // using credential from CustomUser2 entity - End

    // using credential from in memory user detail service Secuity -> userDetailsService() - Start
    @Test
    void testHttpBasicInMemAuthFromInMemUserDetailsEntityUsingUser1() throws Exception{
        mockMvc.perform(get("/students")
                // this will fail because we specified differ credential in main -> application.properties
                //.with(httpBasic("testuser", "testpassword")))
                // this will pass because we specified same credential in main -> application.properties
                .with(httpBasic("inmem", "inmem")))
                .andExpect(status().isOk());
    }
    @Test
    void testHttpBasicInMemAuthFromInMemUserDetailsEntityUsingUser2() throws Exception{
        mockMvc.perform(get("/students")
                // this will fail because we specified differ credential in main -> application.properties
                //.with(httpBasic("testuser", "testpassword")))
                // this will pass because we specified same credential in main -> application.properties
                .with(httpBasic("inmem2", "inmem2")))
                .andExpect(status().isOk());
    }

    // using credential from in memory user detail service Secuity -> userDetailsService() - End
}
